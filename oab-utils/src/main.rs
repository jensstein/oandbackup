extern crate clap;
extern crate libc;
extern crate tar;
extern crate flate2;

use std::fmt;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

use clap::{Arg, App, AppSettings, SubCommand};

use tar::{Archive,Builder};
use flate2::Compression;
use flate2::write::GzEncoder;
use flate2::read::GzDecoder;
use nix::sys::signal::{self,Signal};
use nix::unistd::Pid;

// https://blog.rust-lang.org/2016/05/13/rustup.html
// https://users.rust-lang.org/t/announcing-cargo-apk/5501

#[derive(Debug, Clone)]
pub struct OabError {
    pub message: String
}

impl fmt::Display for OabError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl From<io::Error> for OabError {
    fn from(error: io::Error) -> Self {
        OabError {
            message: error.to_string()
        }
    }
}

// put code related to looking up user and group ids into a trait both to
// facilitate mocking in unit tests and to isolate the os-specific code.
trait IdRetrieverModel {
    fn get_uid(&self, name: &str) -> Result<u32, OabError>;
    fn get_gid(&self, name: &str) -> Result<u32, OabError>;
}

struct IdRetriever {
}
impl IdRetrieverModel for IdRetriever {
    fn get_uid(&self, name: &str) -> Result<u32, OabError> {
        unsafe {
            let name_cstring = str_to_cstring(name)?;
            let mut buf = Vec::with_capacity(512);
            let mut passwd: libc::passwd = std::mem::zeroed();
            let mut result = std::ptr::null_mut();
            match libc::getpwnam_r(name_cstring.as_ptr(), &mut passwd,
                    buf.as_mut_ptr(), buf.capacity(), &mut result) {
                0 if !result.is_null() => Ok((*result).pw_uid),
                _ => Err(OabError {
                        message: format!("no uid found for user {}", name)
                    }
                )
            }
        }
    }

    fn get_gid(&self, name: &str) -> Result<u32, OabError> {
        unsafe {
            let name_cstring = str_to_cstring(name)?;
            // armv7-linux-androideabi doesn't have getgrnam_r so we have to
            // use getgrnam instead
            let group = libc::getgrnam(name_cstring.as_ptr());
            match group.as_ref() {
                Some(group) => Ok((*group).gr_gid),
                None => Err(OabError {
                        message: format!("no gid found for group {}", name)
                    }
                )
            }
        }
    }
}

fn get_owner_ids(path: &Path) -> Result<(u32, u32), std::io::Error> {
    let metadata = fs::metadata(path)?;
    Ok((metadata.uid(), metadata.gid()))
}

fn str_to_cstring(s: &str) -> Result<std::ffi::CString, OabError> {
    match std::ffi::CString::new(s) {
        Ok(s) => Ok(s),
        Err(e) => {
            return Err(OabError {
                    message: e.to_string()
                }
            )
        }
    }
}

fn parse_id_input<M: IdRetrieverModel>(id_retriever: &M, id: &str) ->
        Result<(u32, Option<u32>), OabError> {
    if let Some(index) = id.find(":") {
        let uid_str = &id[..index];
        let uid = match uid_str.parse::<u32>() {
            Ok(uid) => uid,
            Err(_) => id_retriever.get_uid(uid_str)?
        };
        let gid_str = &id[index + 1..];
        let gid = match gid_str.parse::<u32>() {
            Ok(gid) => gid,
            Err(_) => id_retriever.get_gid(gid_str)?
        };
        return Ok((uid, Some(gid)));
    }
    let uid = match id.parse::<u32>() {
        Ok(uid) => uid,
        Err(_e) => id_retriever.get_uid(id)?
    };
    Ok((uid, None))
}


fn change_owner(path: &Path, uid: u32, gid: u32) -> Result<(), OabError> {
    let path_cstring = str_to_cstring(path.to_str().unwrap())?;
    unsafe {
        match libc::chown(path_cstring.as_ptr(), uid, gid) {
            0 => Ok(()),
            _ => Err(OabError {
                    message: format!("unable to change owner of {:?} to {}:{}", path,
                        uid, gid)
                }
            )
        }
    }
}

fn change_owner_recurse(path: &Path, uid: u32, gid: u32) -> Result<(), OabError> {
    change_owner(path, uid, gid)?;
    if path.is_dir() {
        let files = path.read_dir()?;
        for file in files {
            let entry = file?;
            change_owner_recurse(&entry.path(), uid, gid)?;
        }
    }
    Ok(())
}

fn set_permissions(path: &Path, mode: u32) -> Result<(), OabError> {
    let mut perms = match fs::metadata(path) {
        Err(e) => {
            return Err(OabError {
                message: format!("error getting permissions for {:?}: {}", path,
                    e)
            });
        }
        Ok(metadata) => metadata.permissions()
    };
    if perms.mode() & 0o777 != mode {
        perms.set_mode(mode);
        if let Err(e) = std::fs::set_permissions(path, perms) {
            return Err(OabError {
                message: format!("unable to set mode {:o} on path {:?}: {}",
                    mode, path, e)
            });
        }
    }
    Ok(())
}

fn set_permissions_recurse(path: &Path, mode: u32) -> Result<(), OabError> {
    set_permissions(path, mode)?;
    if path.is_dir() {
        let files = path.read_dir()?;
        for file in files {
            let entry = file?;
            set_permissions_recurse(&entry.path(), mode)?;
        }
    }
    Ok(())
}

pub fn strip_leading_path_seperator(path: &Path) -> &Path {
    // Tar files cannot contain absolute directory paths so if the source
    // path is absolute the root should be stripped.
    if path.is_absolute() {
        if let Ok(stripped_path) = path.strip_prefix("/") {
            return stripped_path
        }
    }
    path
}

pub fn archive(archive_path: &Path, source: &Path, follow_symlinks: bool,
        level: u32, strip_path: bool) -> Result<(), OabError> {
    let file = File::create(archive_path)?;
    let enc = GzEncoder::new(file, Compression::new(level));
    let mut builder = Builder::new(enc);
    let path_name = match strip_path {
        true => Path::new(""),
        false => strip_leading_path_seperator(source)
    };
    builder.follow_symlinks(follow_symlinks);
    // This part is largely copied from https://docs.rs/tar/0.4.37/src/tar/builder.rs.html#329-343
    // The reason for not using `append_dir_all` directly like I did before is that it seems to
    // stall if the source archive contains a fifo.
    let mut stack = vec![((source.to_path_buf(), true, false))];
    while let Some((src, is_dir, is_symlink)) = stack.pop() {
        let stripped_source = match src.strip_prefix(&source) {
            Ok(p) => p,
            Err(error) => return Err(OabError {
                message: format!("Error stripping prefix {:?} from {:?}: {}", &source, src, error)
            })
        };
        let dest_path = path_name.join(stripped_source);
        if is_dir || (is_symlink && follow_symlinks && src.is_dir()) {
            for entry in fs::read_dir(&src)? {
                let entry = entry?;
                let file_type = entry.file_type()?;
                // Only try adding regular files, symlinks or directories to the archive. Otherwise
                // we risk getting errors if the source directory contains e.g. fifos.
                if file_type.is_dir() || file_type.is_file() || file_type.is_symlink() {
                    stack.push((entry.path(), file_type.is_dir(), file_type.is_symlink()));
                }
            }
        }
        if src != source {
            builder.append_path_with_name(&src, dest_path)?
        }
    }
    Ok(builder.finish()?)
}

pub fn unpack_archive(archive_path: &Path, destination: &Path) ->
        io::Result<()> {
    let file = File::open(archive_path)?;
    let decoded = GzDecoder::new(file);
    let mut tar_archive = Archive::new(decoded);
    tar_archive.unpack(destination)
}

fn get_pids_for_process_name(name: &str) -> Result<Vec<i32>, OabError> {
    // I would have preffed to use something like `sysinfo` for this task, but it didn't give me
    // useable process names on Android for my specific usecase.
    let mut pids = Vec::new();
    for entry in Path::new("/proc").read_dir()? {
        let entry = entry?;
        if let Ok(pid) = entry.file_name().to_string_lossy().parse::<i32>() {
            let cmdline_path = entry.path().join("cmdline");
            let mut file = File::open(&cmdline_path)?;
            let mut buf = Vec::new();
            file.read_to_end(&mut buf)?;
            // For some reason `read_to_string` reads trailing zeros. That's not ideal when I want
            // to compare with the given process name so I truncate the buffer to
            // the position of the first zero.
            // For some processes this will end up truncating too much of the string since some
            // might have arguments separated by zeros, but for the usecase on Android that doesn't
            // seem to be a problem.
            buf.iter().position(|item| item == &0).and_then(|idx| Some(buf.truncate(idx)));
            if let Ok(cmdline) = std::str::from_utf8(&buf) {
                if cmdline == name {
                    pids.push(pid);
                }
            }
        }
    }
    Ok(pids)
}

fn kill_pid(pid: i32) -> Result<(), OabError> {
    match signal::kill(Pid::from_raw(pid), Signal::SIGINT) {
        Ok(_) => Ok(()),
        Err(error) => Err(OabError{message: error.to_string()})
    }
}

fn main() {
    // https://github.com/kbknapp/clap-rs
    let args = App::new("oab-utils")
        .setting(AppSettings::ArgRequiredElseHelp)
        .subcommand(SubCommand::with_name("owner")
            .arg(Arg::with_name("input")
                .help("file to get info from")
                .required(true)))
        .subcommand(SubCommand::with_name("set-permissions")
            .arg(Arg::with_name("mode")
                .help("mode to set, in octal (e.g. 644)")
                .required(true))
            .arg(Arg::with_name("recursive")
                .short("r")
                .long("recursive")
                .help("set permissions recursively"))
            .arg(Arg::with_name("input")
                .required(true))
        )
        .subcommand(SubCommand::with_name("change-owner")
            .arg(Arg::with_name("id")
                .help("uid and optionally gid to set, separated by :")
                .required(true))
            .arg(Arg::with_name("recursive")
                .short("r")
                .long("recursive")
                .help("change owner of files recursively"))
            .arg(Arg::with_name("path")
                .required(true))
        )
        .subcommand(SubCommand::with_name("archive")
            .arg(Arg::with_name("source")
                .required(true))
            .arg(Arg::with_name("destination")
                .required(true))
            .arg(Arg::with_name("compression-level")
                .short("-c")
                .default_value("6"))
            .arg(Arg::with_name("follow-symlinks")
                .long("--follow-symlinks")
                .short("-L"))
            .arg(Arg::with_name("strip-path")
                .long("strip-path")
                .short("-s"))
        )
        .subcommand(SubCommand::with_name("unpack-archive")
            .arg(Arg::with_name("source")
                .required(true))
            .arg(Arg::with_name("destination")
                .required(true))
        )
        .subcommand(SubCommand::with_name("kill")
            .arg(Arg::with_name("process-name")
                .required(true))
            .arg(Arg::with_name("verbose")
                .short("-v")
                .long("--verbose"))
            .arg(Arg::with_name("die-on-error")
                .long("--die-on-error"))
        )
        .get_matches();
    match args.subcommand() {
        ("owner", Some(args)) => {
            let input = Path::new(args.value_of("input").unwrap());
            match get_owner_ids(input) {
                Ok((uid, gid)) => {
                    println!("{{\"uid\": {}, \"gid\": {}}}", uid, gid);
                },
                Err(e) => {
                    eprintln!("error getting owner ids for {:?}: {}", input,
                        e);
                    std::process::exit(1);
                }
            };
        },
        ("set-permissions", Some(args)) => {
            let input = Path::new(args.value_of("input").unwrap());
            let mode_str = args.value_of("mode").unwrap();
            let mode = match u32::from_str_radix(mode_str, 8) {
                Err(e) => {
                    eprintln!("error parsing input {}: {}", mode_str, e);
                    std::process::exit(1);
                }
                Ok(value) => value
            };
            if args.is_present("recursive") {
                if let Err(err) = set_permissions_recurse(input, mode) {
                    eprintln!("{}", err);
                    std::process::exit(1);
                }
            } else {
                if let Err(err) = set_permissions(input, mode) {
                    eprintln!("{}", err);
                    std::process::exit(1);
                }
            }
        },
        ("change-owner", Some(args)) => {
            let path = Path::new(args.value_of("path").unwrap());
            let id_retriever = IdRetriever {};
            match parse_id_input(&id_retriever, args.value_of("id").unwrap()) {
                Ok((uid, gid_option)) => {
                    let gid = match gid_option {
                        Some(gid) => gid,
                        None => match get_owner_ids(path) {
                            Ok((_, gid)) => gid,
                            Err(e) => {
                                eprintln!("unable to get group id for {:?}: {}",
                                    path, e);
                                std::process::exit(1);
                            }
                        }
                    };
                    if args.is_present("recursive") {
                        if let Err(e) = change_owner_recurse(path, uid, gid) {
                            eprintln!("{}", e);
                            std::process::exit(1);
                        }
                    } else {
                        if let Err(e) = change_owner(path, uid, gid) {
                            eprintln!("{}", e);
                            std::process::exit(1);
                        }
                    }
                },
                Err(e) => eprintln!("unable to parse input: {}", e)
            };
        },
        ("archive", Some(args)) => {
            let source = args.value_of("source").unwrap();
            let destination = args.value_of("destination").unwrap();
            let follow_symlinks = args.is_present("follow-symlinks");
            let strip_prefix = args.is_present("strip-path");
            let compression_level_str = args.value_of("compression-level")
                .unwrap();
            let compression_level: u32 = match compression_level_str.parse() {
                Ok(value) => value,
                Err(_e) => {
                    eprintln!("Error parsing input \"{}\" as a number",
                        compression_level_str);
                    std::process::exit(1);
                }
            };
            if compression_level < 1 || compression_level > 9 {
                eprintln!("Compression level must be between 1 and 9");
                std::process::exit(1);
            }
            if let Err(err) = archive(Path::new(destination),
                    Path::new(source), follow_symlinks, compression_level,
                    strip_prefix) {
                eprintln!("Unable to archive {} to {}: {}", source,
                    destination, err);
                std::process::exit(1);
            }
        },
        ("unpack-archive", Some(args)) => {
            let source = args.value_of("source").unwrap();
            let destination = args.value_of("destination").unwrap();
            if let Err(err) = unpack_archive(Path::new(source),
                    Path::new(destination)) {
                eprintln!("Unable to unpack archive {} to {}: {}", source,
                    destination, err);
                std::process::exit(1);
            }
        },
        ("kill", Some(args)) => {
            let process = args.value_of("process-name").expect("No process specified");
            let die_on_error = args.is_present("die-on-error");
            let verbose = args.is_present("verbose");
            get_pids_for_process_name(process)
                .unwrap_or_else(|error| {
                    eprintln!("Error getting pids for {}: {}", process, error);
                    std::process::exit(1);
                })
                .iter()
                .for_each(|&pid| {
                    if verbose {
                        println!("Killing pid {} for process {}", pid, process);
                    }
                    if let Err(error) = kill_pid(pid) {
                        eprintln!("Unable to kill pid {} for process {}: {}",
                            pid, process, error);
                        if die_on_error {
                            std::process::exit(1);
                        }
                    }
                });
        }
        ("", None) => {
            eprintln!("no commands specified");
            std::process::exit(1);
        },
        _ => unreachable!()
    }
}

#[cfg(test)]
mod test_main;
