extern crate tempfile;

use super::*;
use std::os::unix::fs::MetadataExt;
use std::path::PathBuf;
use std::io::Write;

struct MockIdRetriever {
    mock_uid: u32,
    mock_gid: u32,
    mock_error: Option<String>
}
impl IdRetrieverModel for MockIdRetriever {
    #[allow(unused_variables)]
    fn get_uid(&self, name: &str) -> Result<u32, OabError> {
        if let Some(ref error) = self.mock_error {
            return Err(OabError {message: error.to_string()});
        }
        Ok(self.mock_uid)
    }
    #[allow(unused_variables)]
    fn get_gid(&self, name: &str) -> Result<u32, OabError> {
        if let Some(ref error) = self.mock_error {
            return Err(OabError {message: error.to_string()});
        }
        Ok(self.mock_gid)
    }
}

#[test]
fn test_parse_id_input() {
    let mock_id_retriever = get_mocked_id_retriever(0, 0, None);
    match parse_id_input(&mock_id_retriever, "1000:100") {
        Ok((uid, gid_option)) => {
            assert_eq!(1000, uid);
            assert_eq!(100, gid_option.unwrap());
        }
        Err(_err) => return assert!(false, "error parsing input")
    };
}

#[test]
fn test_parse_id_input_only_uid() {
    let mock_id_retriever = get_mocked_id_retriever(0, 0, None);
    match parse_id_input(&mock_id_retriever, "1000") {
        Ok((uid, gid_option)) => {
            assert_eq!(1000, uid);
            assert_eq!(None, gid_option)
        }
        Err(_err) => return assert!(false, "error parsing input")
    };
}

#[test]
fn test_parse_id_input_parse_username() {
    let mock_id_retriever = get_mocked_id_retriever(1000, 1000, None);
    match parse_id_input(&mock_id_retriever, "user1000") {
        Ok((uid, gid_option)) => {
            assert_eq!(1000, uid);
            assert_eq!(gid_option, None);
        }
        Err(_err) => assert!(false, "shouldn't fail")
    };
}

#[test]
fn test_parse_id_input_parse_groupname() {
    let mock_id_retriever = get_mocked_id_retriever(0, 1000, None);
    match parse_id_input(&mock_id_retriever, "1000:group1000") {
        Ok((uid, gid_option)) => {
            assert_eq!(1000, uid);
            assert_eq!(1000, gid_option.unwrap());
        }
        Err(_err) => assert!(false, "shouldn't fail")
    };
}

#[test]
fn test_parse_id_input_username_groupname() {
    let mock_id_retriever = get_mocked_id_retriever(1000, 1000, None);
    match parse_id_input(&mock_id_retriever, "user1000:group1000") {
        Ok((uid, gid_option)) => {
            assert_eq!(1000, uid);
            assert_eq!(1000, gid_option.unwrap())
        }
        Err(_err) => assert!(false, "shouldn't fail")
    }
}

#[test]
fn test_parse_id_input_error() {
    let mock_id_retriever = get_mocked_id_retriever(150, 0,
        Some("no such user".to_string()));
    match parse_id_input(&mock_id_retriever, "user1000") {
        Ok((_uid, _gid_option)) => assert!(false, "shouldn't succeed"),
        Err(err) => assert_eq!("no such user", err.to_string())
    }
}

fn get_mocked_id_retriever(uid: u32, gid: u32, error: Option<String>) ->
        MockIdRetriever {
    MockIdRetriever {
        mock_uid: uid,
        mock_gid: gid,
        mock_error: error
    }
}

#[test]
fn test_get_owner_ids() {
    if !can_create_file_file() {
        return;
    }
    let file = tempfile::NamedTempFile::new().unwrap();
    let metadata = file.as_file().metadata().unwrap();
    let uid_tempfile = metadata.uid();
    let gid_tempfile = metadata.gid();
    match get_owner_ids(file.path()) {
        Ok((uid, gid)) => {
            assert_eq!(uid, uid_tempfile);
            assert_eq!(gid, gid_tempfile);
        }
        Err(_err) => assert!(false, "shouldn't fail")
    }
}

#[test]
fn test_get_owner_ids_no_such_file() {
    if !can_create_file_file() {
        return;
    }
    let path: PathBuf;
    {
        // create a temporary file and make it go out of scope immediately
        // while copying its path.  this gives us a path to a file which
        // probably doesn't exist when we try to query its metadata (which
        // is what we want to test for). of course, another file with the
        // same path could be created in the meantime and a bug could prevent
        // the file from being removed when the variable goes out of scope.
        let file = tempfile::NamedTempFile::new().unwrap();
        path = file.path().to_owned();
    }
    match get_owner_ids(&path.as_path()) {
        Ok(_) => assert!(false, "should fail"),
        Err(err) => assert_eq!(err.kind(), std::io::ErrorKind::NotFound)
    }
}

#[test]
fn test_archive() {
    if !can_create_file_file() {
        return;
    }
    let source_dir = tempfile::tempdir().unwrap();
    let dest_dir = tempfile::tempdir().unwrap();
    let archive_path = dest_dir.path().join("archive.tar.gz");
    let mut source_file = tempfile::NamedTempFile::new_in(&source_dir).unwrap();
    write!(source_file, "archive test").unwrap();

    if let Err(err) = archive(&archive_path, &source_dir.path(), true, 6, false) {
        println!("Error happened when archiving: {:?}", err);
        assert!(false, "should not fail");
    }
}

#[test]
fn test_unpack_archive() {
    if !can_create_file_file() {
        return;
    }
    let source_dir = tempfile::tempdir().unwrap();
    let dest_dir = tempfile::tempdir().unwrap();
    let archive_path = dest_dir.path().join("archive.tar.gz");
    let mut source_file = tempfile::NamedTempFile::new_in(&source_dir).unwrap();
    write!(source_file, "archive test").unwrap();

    if let Err(err) = archive(&archive_path, &source_dir.path(), true, 6, true) {
        println!("Error happened when archiving: {:?}", err);
        assert!(false, "should not fail");
    }

    let unpack_destination = tempfile::tempdir().unwrap();
    if let Err(err) = unpack_archive(&archive_path, &unpack_destination.path()) {
        println!("Error happened when unpacking archive: {:?}", err);
        assert!(false, "should not fail");
    }
    let expected_path = unpack_destination.path().join(
        source_file.path().strip_prefix(source_dir)
        .expect(&format!("Unable to strip prefix from {:?}", source_file))
    );
    assert!(expected_path.exists(), "expected path does not exist");
    let result_string = fs::read_to_string(expected_path).unwrap();
    assert_eq!(result_string, "archive test");
}

// FIXME: this test doesn't validate that the spawned process is actually killed. I wouldn't be able
// to conclude anything based on the fact that the spawned process isn't present anymore.
#[test]
#[cfg(unix)]
fn test_kill_pid() {
    let child = std::process::Command::new("sleep")
        .arg("10")
        .spawn()
        .expect("Unable to spawn sleep process");
    let pid = child.id() as i32;
    kill_pid(pid).expect(&format!("Unable to kill pid {}", pid));
}

fn can_create_file_file() -> bool {
    match tempfile::tempfile() {
        Ok(_) => true,
        Err(_) => false
    }
}
